import { db } from "~/plugins/firebase"

export const state = () => ({
  chatOpened: false,
  messages: [],
  resultMessage: [],
  no_order: 0,
  isFreshConversation: false,
  replyFromTech: 0,
  techName: ''
})

export const actions = { // asyncronous
  async nuxtServerInit ({ commit }, { req }) {
    // console.log(req.session)
    // this.$cookies.set('session', req.session)
    // if (req.session && req.session.authUser) {
    //   commit('SET_USER', req.session.authUser)
    // }
  },
  async parsingMessage ({ commit }, payload) {
    // console.log(state)
    let data = db.ref('chat/'+payload.no_order)
    let self = this;
    // let res = []
    // state.techName = payload.technician
    data.on('value', function (snapshot) {
      // this.messageList = snapshot.val()
      // console.log(snapshot.val())
      if (snapshot.val() !== null){
        let raw = []
        Object.entries(snapshot.val()).forEach(([key, val]) => {
          let chat = {
            // messageUser: val.messageUser.split('-')[0] === 'user' ? 'me' : val.messageUser,
            messageUser: val.messageUser,
            messageText: val.messageText,
            messageTime: self.$moment.unix(val.messageTime/1000).format('hh:mm a')
          }
          raw.push(chat)
          // console.log(key); // the name of the current key.
          // console.log(val); // the value of the current key.
        });
        // console.log(raw.filter((x) => x.messageUser.split('-')[0] === 'teknisi'))
        commit('parsingMessage', {raw, payload})
        commit('replyFromTech', raw.filter((x) => x.messageUser.split('-')[0] === 'teknisi'))
      }else{
        // console.log(payload.no_order)
        commit('setFirstConversation', payload.no_order)
        // let data = {
        //   messageText: 'Hello',
        //   messageUser: 'user-1',
        //   messageTime: parseInt(self.$moment().format('x'))
        // }
        // db.ref('chat/'+payload.no_order).push(data)
        // console.log('its empty')
      }
    });
  },
}

export const mutations = {
  setFirstConversation(state, payload){
    state.no_order = payload
    state.isFreshConversation = true
  },
  startConversation(state, payload){
    if (state.isFreshConversation){
      let data = {
        messageText: 'Hello',
        messageUser: 'user-1',
        messageTime: parseInt(this.$moment().format('x'))
      }
      db.ref('chat/'+payload.no_order).push(data)
    }
  },
  openChat (state, v) {
    state.chatOpened = v
  },
  replyFromTech(state, payload){
    // console.log(payload.length)
    state.replyFromTech = payload.length
  },
  loadMessages (state, messages) {
    state.messages = messages
  },
  addMessage (state, message) {
    state.messages.push(message)
  },
  replyMessage (state, message) {
    // console.log(message)
    db.ref('chat/'+state.no_order).push(message)
    // state.messages.push(message)
  },
  parsingMessage (state, payload) {
    state.isFreshConversation = false
    state.messages = payload.raw
    state.no_order = payload.payload.no_order
    state.techName = payload.payload.technician
    // console.log(messageRaw)
  },
}
