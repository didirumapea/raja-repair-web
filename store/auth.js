// import config from '~/store/config'
const moment = require('moment')

// STATE AS VARIABLE
export const state = () => ({
  user: null,
  userToken: '',
  isLoggedIn: false,
  headers: null,
  resetEmail: 'example@ex.com'
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async nuxtServerInit ({ commit }, { req }) {
    // console.log(req.session)
    // this.$cookies.set('session', req.session)
    // if (req.session && req.session.authUser) {
    //   commit('SET_USER', req.session.authUser)
    // }
  },
  async checkEmail ({ commit }, payload) {
    // console.log(payload)
    return await this.$axios.$get('auth/user/check-email/email='+payload)
      .then((response) => {
        // console.log(response)

        return response
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async loginUser ({ commit }, payload) {
    // console.log(payload)
    return await this.$axios.post('auth/user/login', payload)
      .then((response) => {
        // console.log(response)
        if (response.data.success){
          // this.$toast.success('Login Successfully')
          commit('setUser', response.data.data)
        }
        // console.log(response)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async loginUserWithGoogle ({ commit }, payload) {
    // console.log(payload)
    return await this.$axios.post('auth/user/login-social', payload)
      .then((response) => {
        // console.log(response.data.message)
        if (response.data.success){
          // this.$toast.success('Login Successfully')
          commit('setUser', response.data.data)
        }
        // console.log(response)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async registerUser ({ commit }, payload) {
    // console.log(payload)
    await this.$axios.post('auth/user/register', payload)
      .then((response) => {
        //console.log(response)
        if (response.data.success){
          // commit('setUser', response.data.data)
          this.$toast.success('Login Successfully')
          this.app.router.push('/')
          commit('setUser', response.data.data)
        }else{
          this.$toast.error('Login Failed : '+response.data.message)

        }

        // console.log(response)
        // return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async editUser ({ commit }, payload) {
    // console.log(payload)
    let _data = {
      email: payload.email,
      phone: payload.phone,
      living: payload.living,
      gender: payload.gender,
      photo: payload.photo
    }
    const headers = {
      'Authorization': 'Bearer '+this.$cookies.get('session-user').token
    }
    return await this.$axios.post('auth/user/edit', _data, {headers: headers})
      .then((response) => {
        // console.log(response)
        // console.log(response)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async editName ({ commit }, payload) {
    console.log(payload)
    await this.$axios.post(`auth/user/edit/name=${payload}`, {headers: 'Bearer '+this.$cookies.get('session-user').token})
      .then((response) => {
        // console.log(response)
        // console.log(response)
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async detailsUser ({ commit }, payload) {
    // console.log(payload)
    await this.$axios.get('auth/user/detail', {headers: 'Bearer '+this.$cookies.get('session-user').token})
      .then((response) => {
        console.log(response)
        if (response.data.success){
          // this.$toast.success('Login Successfully')
          commit('updateUser', response.data.data)
        }

        // console.log(response)
        // return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async resetPassword ({ commit }, payload) {
    // console.log(payload)
    return await this.$axios.post('auth/user/reset-password', {email: payload})
      .then((response) => {
        // console.log(response)
        // if (response.data.success){
          // this.$toast.success('Login Successfully')
          commit('setEmail', payload)
        // }
        // console.log(response)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async changePassword ({ commit }, payload) {
    let data = {
      email: payload.email,
      old_password: payload.oldPassword,
      new_password: payload.newPassword
    }
    return await this.$axios.post('auth/user/change-password', data)
      .then((response) => {
        // console.log(response)
        // if (response.data.success){
        // this.$toast.success('Login Successfully')
        // commit('setEmail', payload)
        // }
        // console.log(response)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  updateUser(state, payload){
    state.user = payload
  },
  setEmail(state, payload){
    state.resetEmail = payload
  },
  deleteEmail(state, payload){
    state.resetEmail = 'example@ex.com'
  },
  setUser(state, payload){
    //console.log(payload)
    let dt = new Date(moment().add(30, 'd').format())
    let dtToken = new Date(moment().add(30, 'm').format())
    state.isLoggedIn = true;
    state.user = payload
    state.headers = {
      'Authorization': 'Bearer '+payload.token
    }
    // console.log(state)
    this.$cookies.set('session-user', payload, {
      path: '/',
      expires: dt
    })
    this.$cookies.set('session-token', payload, {
      path: '/',
      expires: dtToken
    })
  },
  checkUser(state){
    if (this.$cookies.get('session-user') === undefined || this.$cookies.get('session-user') === ''){
      //console.log('its undefined')
      state.isLoggedIn = false
    }else{
      state.headers = {
        'Authorization': 'Bearer '+this.$cookies.get('session-user').token
      }
      state.user = this.$cookies.get('session-user')
      state.isLoggedIn = true
    }
    // console.log(this.$cookies.get('session-ussr'))
  },
  logoutUser(state){
    this.$cookies.removeAll()
    state.user = null
    state.isLoggedIn = false
  },
  setBanner(state, payload){
    // console.log(payload)
    state.listCategory = payload
    // this.$cookies.get('banner-top', payload)
  },
  increment (state, payload) {
    state.count+=payload
  },
  add (state, text) {
    state.list.push({
      text: text,
      done: false
    })
  },
  remove (state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle (state, todo) {
    todo.done = !todo.done
  }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  listCategory: state => {
    return state.listCategory
  },

}
