// import config from '~/store/config'

// STATE AS VARIABLE
export const state = () => ({
  paymentStatus: [],
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async nuxtServerInit({ commit }, { req }){
    // commit()
    // console.log('server init')
  },
  async getVoucherNotUsed ({ commit }) {
    await this.$axios.$get(`user/myvoucher/trans-status=settlement/operator==/is-used=0/page=1/limit=100/sort=desc/tz-offset=${this.state.setting.timezone}`)
      .then((response) => {
        // console.log(this.state.setting.timezone)
        commit('setlistVoucherNotUsed', response)
        // return response
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  // USED VOUCHER
  async postUseVoucher ({ commit }, payload) {
    // console.log(payload)
    await this.$axios.post(`user/myvoucher/used-voucher/id=${payload}`)
      .then((response) => {
        // console.log(response)
        // commit('setlistVoucherUsed', response)
        // return response
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  setlistVoucherNotUsed(state, payload){
    // console.log(payload)
    state.listVoucherNotUsed = payload
    // this.$cookies.get('banner-top', payload)
  },
  setlistVoucherUsed(state, payload){
    // console.log(payload)
    state.listVoucherUsed = payload
    // this.$cookies.get('banner-top', payload)
  },
  setlistVoucherNotPay(state, payload){
    // console.log(payload)
    state.listVoucherNotPay = payload
    // this.$cookies.get('banner-top', payload)
  },
  useVoucher (state, id) {
    // console.log(state.listVoucherNotUsed.data, id)
    const filteredItems = state.listVoucherNotUsed.data.filter(item => item.id !== id)
    state.listVoucherNotUsed.data = filteredItems
  },
  voucherTimeExpired (state, id) {
    // console.log(state.listVoucherNotUsed.data, id)
    const filteredItems = state.listVoucherNotPay.data.filter(item => item.id !== id)
    state.listVoucherNotPay.data = filteredItems
  },
  // SAMPLE
  increment (state, payload) {
    state.count+=payload
  },
  add (state, text) {
    state.list.push({
      text: text,
      done: false
    })
  },
  remove (state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle (state, todo) {
    todo.done = !todo.done
  }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  listCategory: state => {
    return state.listCategory
  },

}
