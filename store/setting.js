let urlCdn = 'https://cdn.rimember.id/';
import moment from "moment";
// let urlCdn = 'http://localhost:3003/';
// STATE AS VARIABLE
export const state = () => ({
  pathImgMemberImages: urlCdn + 'web-images/member-images/',
  pathImgPromoMerchant: urlCdn + 'web-images/promo-merchant/',
  pathImgCategory: urlCdn + 'web-images/category/',
  pathImgBanner: urlCdn + 'web-images/banner/',
  pathImgMerchant: urlCdn + 'web-images/merchant/',
  timezone: moment().utcOffset()/60,
  version_control: `1.4`
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async getFeatured1 ({ commit }) {
    return await this.$axios.$get('home/product/type=1')
      .then((response) => {
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))

        if (err.response === undefined){
          return 'no connection'
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
        //   return err.response
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  d1(state, payload){
    // console.log(payload)
    state.isTabActive = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  pathImgPromoMerchant: state => {
    return state.pathImgPromoMerchant
  },
}
