// import config from '~/store/config'

// STATE AS VARIABLE
export const state = () => ({
  listOrder: null,
  listDetailOrder: [],
  isUploadSuccess: false,
  scName: ''
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async nuxtServerInit({ commit }, { req }){
    // commit()
    // console.log('server init')
  },
  async setListOrderUser ({ commit }) {
    return await this.$axios.get(`users/get/order/page=1/limit=10/sort=desc`, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async checkout ({ commit }, payload) {
    return await this.$axios.post(`payment/order`, payload, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        // commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
          return {
            success: false,
            message: err,
          }
        }
      })
  },
  async order ({ commit }, payload) {
    // console.log(payload)
    return await this.$axios.post(`users/order`, payload, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        // commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
          return {
            success: false,
            message: err,
          }
        }
      })
  },
  async uploadFiles ({ commit }, payload) {
    return await this.$axios.post(`apps/upload-file/multiple`, payload, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        // commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
          return {
            success: false,
            message: err,
          }
        }
      })
  },
  async cancelOrder ({ commit }, payload) {
    return await this.$axios.post(`users/update-order`, payload, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        // commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
          return {
            success: false,
            message: err,
          }
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  setScName(state, payload){
    state.scName = payload
  },
  setListOrderUser(state, payload){
    // console.log(payload)
    state.listOrder = payload.data
    // this.$cookies.get('banner-top', payload)
  },
  setListDetailOrderUser(state, payload){
    // console.log(payload)
    state.listDetailOrder.push(payload)
    // this.$cookies.get('banner-top', payload)
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  listCategory: state => {
    return state.listCategory
  },
}
