const debug = process.env.NODE_ENV !== 'production'
export const strict = debug
export const state = () => ({
  noImage: require('@/assets/img/no-img.png'),
  web: {
    home: 'Sudirman No. 146, Jakarta',
    phone2: 'Help: + 62 818 4113 6251',
    twitter: 'https://twitter.com/twitter',
    facebook: 'https://facebook.com/facebook',
    pinterest: 'https://pinterest.com/pinterest',
    google: 'https://google.com/google',
    dribbble: '',
    phone: '62 818 4113 6251',
    address: 'Sudirman No. 146',
    city: 'Jakarta',
    email: 'info@rajarepair.com',
    hotline: '(021) 555-0312',
    rr_desc: 'The biggest phone repair in indonesia, solution for your problem phone error.'
  }
})

export const mutations = {
  SET_MODAL_VIEW (state, v) {
  }
}

export const actions = {
  async LOGIN (context, params) {
    const r = await this.$axios.$post(context.state.api + 'login', params).then(res => res)
    return r
  }
}
