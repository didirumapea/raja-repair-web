module.exports = {
  apps : [
    {
      name: "rr-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "rr-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "rr-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
