
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: ' %s - Raja Repair',//pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,800,900' },
      { rel: 'stylesheet', href: '/lib/animate.css/animate.css' },
      { rel: 'stylesheet', href: '/lib/fontawesome/css/all.css' },
      { rel: 'stylesheet', href: '/lib/chosen/chosen.min.css' },
      { rel: 'stylesheet', href: '/lib/jquery-ui-custom/jquery-ui.min.css' },
      { rel: 'stylesheet', href: '/lib/pentix/css/pentix.css' },
      { rel: 'stylesheet', href: '/lib/css/pex-theme.css' }
    ],
    script: [
      { src: '/lib/js/jquery-1.12.4.js', body: true },
      { src: '/lib/parallax.js/parallax.js', body: true },
      { src: '/lib/flexslider/jquery.flexslider-min.js', body: true },
      { src: '/lib/owlcarousel2/owl.carousel.min.js', body: true },
      { src: '/lib/shuffle/shuffle.min.js', body: true },
      { src: '/lib/waypoints/jquery.waypoints.min.js', body: true },
      { src: '/lib/chosen/chosen.jquery.min.js', body: true },
      { src: '/lib/jquery-ui-custom/jquery-ui.min.js', type: 'text/javascript', body: true },
      { src: '/lib/pentix/js/pentix.js', type: 'text/javascript', body: true }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/main.css',
    '@/assets/scss/apps.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/components.js',
    {src: '~plugins/vue-modal', ssr: true},
    '~/plugins/global-function.js',
    '~/plugins/axios.js',
    {src: '~plugins/vue-pagination.js', ssr: false},
    {src: '~plugins/vue-file-upload.js', ssr: false},
    {src: '~plugins/vee-validate.js', ssr: false},
    {src: '~plugins/vue-chat.js', ssr: false},
    // {src: '~plugins/vue-quick-chat.js', ssr: false},
    '~plugins/maps.js',
    { src: '~/plugins/script.js', ssr: false },
    '~/plugins/firebase.js',
    '~/plugins/vue-loading-overlay.js',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/moment',
    'cookie-universal-nuxt',
    ['@nuxtjs/pwa', {
      baseURL: process.env.NODE_ENV === 'production' ? 'https://rbcpro.co' : 'http://localhost:3000'
    }],
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    // ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    // baseURL: 'http://api.edutore.com:3000/api/v1/'
    // baseURL: 'https://api.edutore.com/api/v1/',
    // baseURL: 'https://10.148.0.6:3000/api/v1/'
    baseURL: process.env.NODE_ENV !== "production"
      ? 'http://localhost:3011/api/v1/' // local prod
      // ? 'https://api.rajarepair.id/api/v1/' // local staging
      // ? 'https://api.rimember.id/rimv2/'
      : 'https://api.rajarepair.id/api/v1/',
    // : 'http://localhost:3010/api/v2/'
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },
  server: {
    port: 3022, // default: 3000
    host: '0.0.0.0', // default: localhost,
  },
  manifest: {
    name: 'Raja Repair Website',
    lang: 'id',
    short_name: 'Raja Repair',
    start_url: '/',
    display: 'standalone',
    background_color: '#ffffff',
    theme_color: '#049b93',
    orientationSection: 'portrait',
    description: 'Raja Repair Desc'
    // gcm_sender_id: '482941778795',
    // gcm_sender_id_comment: 'Do not change the GCM Sender ID'
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  generate: {
    dir: "public"
  }
}
