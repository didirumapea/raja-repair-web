import Vue from 'vue'
import { ValidationProvider, extend } from 'vee-validate';

// Register it globally
// main.js or any entry file.
Vue.component('ValidationProvider', ValidationProvider);

// RULES

extend('required', {
  validate (value) {
    return {
      required: true,
      valid: ['', null, undefined].indexOf(value) === -1
    };
  },
  // This rule reports the `required` state of the field.
  message: 'This field is required',
  computesRequired: true
});
extend('email', {
  validate (value) {
    let mailformat = /^\w+([.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return {
      required: true,
      valid: value.match(mailformat)
    };
  },
  message: 'Invalid email format',
  computesRequired: true
});

extend('size', {
  validate (value, args) {
    // console.log(value)
    return {
      // required: true,
      valid:  args.size > 200
    };
  },
  params: ['size'],
  message: 'Invalid email format',
  computesRequired: true
});
