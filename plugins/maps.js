import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA5eOIrjCQIpf8bivwzkHAjmMRUCG9NNlA',
    libraries: 'places'
    // v: 'GOOGLE_MAPS_VERSION'
  }
})
