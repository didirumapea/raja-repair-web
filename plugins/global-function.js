import Vue from 'vue'
import moment from 'moment'

const changeCase = require('change-case')
const uuidv1 = require('uuid/v1');
const uuidv5 = require('uuid/v5');

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
Vue.mixin({
  methods: {
    generateUUID(){
      const v1options = {
        node: [0x01, 0x23, 0x45, 0x67, 0x89, 0xab],
        clockseq: 0x1234,
        msecs: new Date().getTime(),
        nsecs: 2020
      };
      // uuidv1(v1options); // ⇨ '710b962e-041c-11e1-9234-0123456789ab'
      const MY_NAMESPACE = uuidv1(v1options);
       // ⇨ '630eb68f-e0fa-5ecc-887a-7c7a62614681'
      return uuidv5('Raja Repair 2020 Reserved', MY_NAMESPACE)
    },
    statusOrder(status){
      switch (status) {
        case 'pending':
          return 'pending'
        case 'progress':
          return 'processing'
        case 'repair':
          return 'failed'
        case 'complete':
          return 'completed'
      }
    },
    capitalFormat(text){
      return changeCase.capitalCase(text)
    },
    dateDotsFormat(date){
      return this.$moment(date).add(this.$store.state.setting.timezone, 'h').format('DD.MM.YYYY');
    },
    groupBys (xs, key) {
      return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    },
    statusTransaksi(status){
      switch (status) {
        case 'settlement':
          return 'Berhasil'
        case 'pending':
          return 'Diproses'
        case 'failure':
          return 'Gagal'
        case 'unverification':
          return 'Diproses'
      }
    },
    parseDateTrans(date){
      return this.$moment(date).format('ll')
    },
    classV(index){
      if (index === 0){
        return 'color-row border'
      }else{
        return 'color-row border mt-2'
      }
    },
    classMyTransaction(index){
      let _color = '-grey'
      if(index & 1)
      {
        // ODD
        _color = ''
      }
      else
      {
        // EVEN
        _color = '-grey'
      }

      if (index === 0){
        return 'color-grey-row border'
      }else{
        return `color${_color}-row border mt-3`
      }


    },
    convertIsoToDate(_date){
      // let date = new Date('2013-08-03T02:00:00Z');
      // let year = date.getFullYear();
      // let month = date.getMonth()+1;
      // let dt = date.getDate();
      // var str = '2011-04-11T10:20:30Z';
      let date = moment(_date);
      // let dateComponent = date.utc().format('DD MMMM YYYY');
      // let timeComponent = date.utc().format('HH:mm:ss');
      // console.log(dateComponent);
      // console.log(timeComponent);
      return date.utc().format('DD MMM YYYY');
    },
    GzeroFill(val){
      let n = val
      let l = 7
      return ('0000000000'+n).slice(-l);
    },
    currencyFormat(n, currency){
      // WITH COMMA DIVIDER
      return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
      });
    },
    currencyFormat2(n, currency){
      // WITH DOTS DIVIDER
      return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
      });
    },
    htmlEntitiesConverter(htmlEntities){
      // console.log(entities.decode('&lt;&gt;&quot;&apos;&amp;&copy;&reg;&#8710;')); // <>"'&&copy;&reg;∆
      return entities.decode(htmlEntities)
    },
    groupBy(list, keyGetter) {
      const map = new Map();
      list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
          map.set(key, [item]);
        } else {
          collection.push(item);
        }
      });
      return map;
    },
    getTgl(){
      let tgl = []
      for (let x = 1; x <= 31; x++){
        tgl.push(x)
      }
      // console. log(tgl)
      return tgl
    },
    getBulan(){
      return ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    },
    getTahun(){
      let dt = new Date()
      // console.log(dt.getFullYear())
      let thn = []
      for (let x = dt.getFullYear(); x >= dt.getFullYear()-100; x--){
        thn.push(x)
      }
      // console.log(thn)
      return thn
    },
    logoutUser(){
      // console.log(this.$store.state.auth.user.token)
      this.$nuxt.$router.push('/logout?user?'+this.$store.state.auth.user.token)
      this.$store.commit('auth/logoutUser')
    }
  }
})
