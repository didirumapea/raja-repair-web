import Vue from 'vue'

const Headers = () => import(/* webpackChunkName: "header" */ '@/components/core/Headers.vue')
const MenuNav = () => import(/* webpackChunkName: "menu-nav" */ '@/components/core/MenuNav.vue')
const Cart = () => import(/* webpackChunkName: "cart" */ '@/components/core/Cart.vue')
const Footer = () => import(/* webpackChunkName: "footer" */ '@/components/core/Footer.vue')
const LoaderBlock = () => import(/* webpackChunkName: "loader-block" */ '@/components/core/LoaderBlock.vue')
const ScrollToTop = () => import(/* webpackChunkName: "scroll-to-top" */ '@/components/core/ScrollToTop.vue')
const Search = () => import(/* webpackChunkName: "search" */ '@/components/core/Search.vue')
const ProfileNav = () => import(/* webpackChunkName: "profile-nav" */ '@/components/core/ProfileNav.vue')
const Maps = () => import(/* webpackChunkName: "maps" */ '@/components/core/Maps.vue')
const MapsOrder = () => import(/* webpackChunkName: "maps" */ '@/components/core/MapsOrder.vue')
const Chat = () => import(/* webpackChunkName: "maps" */ '@/components/core/Chat.vue')

// ADDITIONAL COMPONENTS
const Header1 = () => import(/* webpackChunkName: "header1" */ '@/components/core/Header1.vue')
const Header2 = () => import(/* webpackChunkName: "header2" */ '@/components/core/Header2.vue')
const Header3 = () => import(/* webpackChunkName: "header3" */ '@/components/core/Header3.vue')
const Header4 = () => import(/* webpackChunkName: "header4" */ '@/components/core/Header4.vue')
const Header5 = () => import(/* webpackChunkName: "header5" */ '@/components/core/Header5.vue')
const Footer1 = () => import(/* webpackChunkName: "footer1" */ '@/components/core/Footer1.vue')
const Footer2 = () => import(/* webpackChunkName: "footer2" */ '@/components/core/Footer2.vue')
const Footer3 = () => import(/* webpackChunkName: "footer3" */ '@/components/core/Footer3.vue')

Vue.component('Headers', Headers)
Vue.component('MenuNav', MenuNav)
Vue.component('Cart', Cart)
Vue.component('Footer', Footer)
Vue.component('LoaderBlock', LoaderBlock)
Vue.component('ScrollToTop', ScrollToTop)
Vue.component('Search', Search)
Vue.component('ProfileNav', ProfileNav)
Vue.component('Maps', Maps)
Vue.component('MapsOrder', MapsOrder)
Vue.component('Chat', Chat)

// ADDITIONAL COMPONENTS
Vue.component('Header1', Header1)
Vue.component('Header2', Header2)
Vue.component('Header3', Header3)
Vue.component('Header4', Header4)
Vue.component('Header5', Header5)
Vue.component('Footer1', Footer1)
Vue.component('Footer2', Footer2)
Vue.component('Footer3', Footer3)

Vue.prototype.$copyText = function (element) {
  let range
  let selection
  if (document.body.createTextRange) {
    range = document.body.createTextRange()
    range.moveToElementText(element)
    range.select()
  } else if (window.getSelection) {
    selection = window.getSelection()
    range = document.createRange()
    range.selectNodeContents(element)
    selection.removeAllRanges()
    selection.addRange(range)
  }
  try {
    return document.execCommand('copy')
  } catch (err) {
    return err
  }
}

Vue.prototype.$setTitle = function (str) {
  if (str) {
    return (str.charAt(0).toUpperCase() + str.slice(1)).replace('-', ' ')
  } else {
    return '-'
  }
}

Vue.prototype.$strLimit = function (str, limit) {
  if (str && parseInt(limit)) {
    return str.substring(0, limit) + (str.length > limit ? '..' : '')
  } else {
    return str.substring(0, 10)
  }
}

Vue.prototype.$price = function (price) {
  if (parseInt(price)) {
    return 'Rp' + parseInt(price).toLocaleString()
  } else {
    return '-'
  }
}

Vue.prototype.$myTransactionUnpaid = function (transactions) {
  /* eslint-disable */
  let total = 0
  transactions.filter((r) => {
    if (parseInt(r.status) === 0) {
      total++
    }
  })
  return total
}

Vue.prototype.$uuid = function () {
  const navigatorInfo = window.navigator
  const screenInfo = window.screen
  let uid = navigatorInfo.mimeTypes.length
  uid += navigatorInfo.userAgent.replace(/\D+/g, '')
  uid += navigatorInfo.plugins.length
  uid += screenInfo.height || ''
  uid += screenInfo.width || ''
  uid += screenInfo.pixelDepth || ''
  return uid
}

Vue.prototype.$device = function () {
  return (navigator.userAgent.match(/iPad/i)) === 'iPad' ? 'ios' : (navigator.userAgent.match(/iPhone/i)) === 'iPhone' ? 'ios' : (navigator.userAgent.match(/Android/i)) === 'Android' ? 'android' : (navigator.userAgent.match(/BlackBerry/i)) === 'BlackBerry' ? 'blackberry' : null
}

Vue.prototype.$totalWeight = function (mycarts) {
  if (mycarts) {
    let tWeight = 0
    mycarts.map((r) => {
      tWeight += (r.berat * r.jumlah)
    })
    tWeight = Math.ceil(tWeight / 1000)
    return tWeight
  } else {
    return '-'
  }
}

Vue.prototype.$getTotalPrice = function (carts) {
  if (typeof carts === 'object') {
    let totalHarga = 0
    carts.map((r) => {
      totalHarga += (parseInt(r.discount) ? parseInt(r.discount) : parseInt(r.harga)) * parseInt(r.jumlah)
    })
    return totalHarga
  } else {
    return 0
  }
}
Vue.prototype.$getTotalPriceReseller = function (carts, resellerPrice) {
  if (typeof carts === 'object') {
    let totalHarga = 0
    carts.map((r) => {
      const afterDiscount = parseInt(r.discount) ? parseInt(r.discount) : parseInt(r.harga)
      const afterDiscountReseller = afterDiscount - (afterDiscount * resellerPrice / 100)
      totalHarga += afterDiscountReseller * parseInt(r.jumlah)
    })
    return totalHarga
  } else {
    return 0
  }
}

Vue.prototype.$reformatTanggal = function (tanggal) {
  if (tanggal && typeof tanggal === 'string') {
    return tanggal.substr(0, 10)
  } else {
    return '-'
  }
}

Vue.prototype.$getStatus = function (status) {
  status = parseInt(status)
  switch (status) {
    case 0:
      return 'unpaid'
    case 1:
      return 'pending'
    case 2:
      return 'processed'
    case 3:
      return 'shipping'
    case 4:
      return 'complete'
    case 99:
      return 'canceled'
    default:
      return '-'
  }
}

Vue.prototype.$imgtoBase64 = function (event, callback) {
  /* eslint-disable */
  function getType(ext) {
    if (ext === 'mov' || ext === 'mp4' || ext === 'avi' || ext === 'flv') {
      return 'video'
    } else if (ext === 'doc' || ext === 'docx' || ext === 'ppt' || ext === 'pptx' || ext === 'xls' || ext === 'xlsx' || ext === 'csv' || ext === 'txt' || ext === 'pdf' || ext === 'psd') {
      return 'doc'
    } else if (ext === 'jpg' || ext === 'jpeg' || ext === 'gif' || ext === 'png' || ext === 'svg') {
      return 'photo'
    } else if (ext === 'mp3' || ext === 'wav') {
      return 'audio'
    } else {
      return 'unknown'
    }
  }
  let r = {
    status: false,
    ext: '',
    type: '',
    data: null
  }
  let f = event.target.files || event.dataTransfer.files

  const reader = new FileReader()
  if (f[0]) {
    const fname = event.target.files[0].name
    const lastDot = fname.lastIndexOf('.')
    r.ext = fname.substring(lastDot + 1)
    r.type = getType(r.ext)

    const fSize = r.ext === 'mov' || r.ext === 'mp4' || r.ext === 'avi' || r.ext === 'flv' ? 20000000 : 3000000 // 10MB : 3MB

    if (f[0].size <= fSize) {
      reader.readAsDataURL(f[0])
      reader.onload = function (ev) {
        r.status = true
        r.data = ev.target.result
        callback(r)
      }
      reader.onerror = function (error) {
        r.status = false
        r.data = error
        callback(r)
      }
    } else {
      r.status = false
      r.data = 'file_size'
      callback(r)
    }
  } else {
    r.status = false
    r.data = 'canceled'
    callback(r)
  }
}

Vue.prototype.$getExt = function (str) {
  const lastDot = str.lastIndexOf('.')
  return str.substring(lastDot + 1)
}

Vue.prototype.$correctWhatsapp = function (phone) {
  if (!phone) {
    return false
  }
  if (phone.substring(0, 1) === '8') {
    phone = '628' + phone.substring(1, phone.length)
  }
  if (phone.substring(0, 1) === '0') {
    phone = '62' + phone.substring(1, phone.length)
  }
  if (phone.substring(0, 3) === '+62') {
    phone = phone.replace('+62', '62')
  }
  return phone
}

Vue.prototype.$NumberOnly = function (e) {
  const key = e.keyCode ? e.keyCode : e.which
  if (isNaN(String.fromCharCode(key)) && key !== 8 && key !== 46 && key !== 37 && key !== 39) {
    e.preventDefault()
    return false
  }
}

Vue.prototype.$validateEmail = function (email) {
  // eslint-disable-next-line
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

Vue.prototype.$reformatPhone = function (phone) {
  if (phone) {
    if (phone.substring(0, 1) === '8') {
      return '62' + phone
    } else if (phone.substring(0, 2) === '08') {
      return '62' + phone.substring(1, phone.length)
    } else if (phone.substring(0, 2) === '62') {
      return phone
    } else if (phone.substring(0, 3) === '+62') {
      return phone.substring(1, phone.length)
    } else {
      return false
    }
  } else {
    return false
  }
}

Vue.prototype.$validatePhone = function (phone, arePhone) {
  if (phone) {
    if (phone.length > 7 && phone.length < 15) {
      if (arePhone === '0' && phone.length > 9 && phone.length < 14) {
        if (phone.substring(0, 1) === '0') {
          return true
        } else {
          return false
        }
      } else if (arePhone) {
        if (phone.substring(0, 1) === '2' || phone.substring(0, 2) === '02' || phone.substring(0, 2) === '62' || phone.substring(0, 3) === '+62') {
          return true
        } else {
          return false
        }
      } else if (phone.substring(0, 1) === '8' || phone.substring(0, 2) === '08' || phone.substring(0, 2) === '62' || phone.substring(0, 3) === '+62') {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  } else {
    return false
  }
}

Vue.prototype.$convertTime = function (str) {
  const date = new Date(str)
  return date.toString()
}

Vue.prototype.$setTime = function (str) {
  const date = new Date(str)
  const year = date.getFullYear()
  const month = parseInt(date.getMonth()) < 10 ? '0' + (parseInt(date.getMonth()) + 1) : date.getMonth()
  const day = parseInt(date.getDate()) < 10 ? '0' + parseInt(date.getDate()) : date.getDate()
  const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
  const minute = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
  const seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  const newDate = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + seconds
  return newDate.substr(11, 5)
}
